# Generated by Django 2.0.3 on 2018-05-07 09:15

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('master', '0029_auto_20180507_1301'),
    ]

    operations = [
        migrations.RenameField(
            model_name='companykeyperson',
            old_name='company_key_person_designation',
            new_name='designation',
        ),
        migrations.RenameField(
            model_name='companykeyperson',
            old_name='company_key_person_email_id',
            new_name='email_id',
        ),
        migrations.RenameField(
            model_name='companykeyperson',
            old_name='company_key_person_mobile_number',
            new_name='mobile_number',
        ),
        migrations.RenameField(
            model_name='companykeyperson',
            old_name='company_key_person_name',
            new_name='name',
        ),
        migrations.RemoveField(
            model_name='product',
            name='tag',
        ),
        migrations.AlterField(
            model_name='distributor',
            name='gst_number',
            field=models.CharField(max_length=255, validators=[django.core.validators.RegexValidator(message='Invalid GST Number (NNCCCCCNNNNCXZX)', regex='^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$')]),
        ),
    ]
