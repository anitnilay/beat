# Generated by Django 2.0.3 on 2018-05-16 06:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('master', '0033_auto_20180515_2336'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='bank',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='master.BankDetail'),
        ),
        migrations.AlterField(
            model_name='company',
            name='district',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='master.District'),
        ),
        migrations.AlterField(
            model_name='company',
            name='state',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='master.State'),
        ),
        migrations.AlterField(
            model_name='designation',
            name='reporting_designation',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='master.Designation'),
        ),
        migrations.AlterField(
            model_name='distributor',
            name='bank',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='master.BankDetail'),
        ),
        migrations.AlterField(
            model_name='distributor',
            name='district',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='master.District'),
        ),
        migrations.AlterField(
            model_name='distributor',
            name='field_staff',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='distributor_field_staff', to='master.Employee'),
        ),
        migrations.AlterField(
            model_name='distributor',
            name='state',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='master.State'),
        ),
        migrations.AlterField(
            model_name='district',
            name='state',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='master.State'),
        ),
        migrations.AlterField(
            model_name='employee',
            name='bank',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='master.BankDetail'),
        ),
        migrations.AlterField(
            model_name='employee',
            name='designation',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='master.Designation'),
        ),
        migrations.AlterField(
            model_name='employee',
            name='district',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='master.District'),
        ),
        migrations.AlterField(
            model_name='employee',
            name='headquarter_district',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='headquarter_district', to='master.District'),
        ),
        migrations.AlterField(
            model_name='employee',
            name='headquarter_state',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='headquarter_state', to='master.State'),
        ),
        migrations.AlterField(
            model_name='employee',
            name='reporting_to',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='reporting_to_name', to='master.Employee'),
        ),
        migrations.AlterField(
            model_name='employee',
            name='state',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='master.State'),
        ),
        migrations.AlterField(
            model_name='product',
            name='group',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='master.ProductGroup'),
        ),
        migrations.AlterField(
            model_name='product',
            name='unit',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='master.Unit'),
        ),
        migrations.AlterField(
            model_name='town',
            name='district',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='master.District'),
        ),
        migrations.AlterField(
            model_name='town',
            name='state',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='master.State'),
        ),
    ]
