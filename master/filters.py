from django_filters import FilterSet, CharFilter
from master import models
from django.db.models import CharField
from django.db.models import Q



class TownFilter(FilterSet):

    class Meta:
        model = models.Town
        fields = {'name': ['icontains']}


class CompanyFilter(FilterSet):

    class Meta:
        model = models.Company
        fields = {'firm_name': ['icontains']}


class DatesManagerFilter(FilterSet):

    class Meta:
        model = models.DatesManager
        fields = {'start_date': ['icontains']}


class StateFilter(FilterSet):

    class Meta:
        model = models.State
        fields = {'name': ['icontains']}


class DistrictFilter(FilterSet):

    class Meta:
        model = models.District
        fields = {'name': ['icontains']}


class UnitFilter(FilterSet):

    class Meta:
        model = models.Unit
        fields = {'name': ['icontains']}


class ProductGroupFilter(FilterSet):

    name = CharFilter(name='name', method='filter_name')

    class Meta:
        model = models.ProductGroup
        fields = ('name',)

    def filter_name(self, queryset, name, value):
        fields = [f for f in self.Meta.model._meta.get_fields() if isinstance(
            f, CharField
        )]
        queries = [Q(**{f.name + '__icontains': value}) for f in fields]
        qs = Q()
        for query in queries:
            qs = qs | query
        return self.Meta.model.objects.filter(qs)


class ProductFilter(FilterSet):

    name = CharFilter(name='name', method='filter_name')

    class Meta:
        model = models.Product
        fields = {'name'}

    def filter_name(self, queryset, name, value):
        fields = [f for f in self.Meta.model._meta.get_fields() if isinstance(
            f, CharField
        )]
        queries = [Q(**{f.name + '__icontains': value}) for f in fields]
        qs = Q()
        for query in queries:
            qs = qs | query
        return self.Meta.model.objects.filter(qs)


class DesignationFilter(FilterSet):

    name = CharFilter(name='name', method='filter_name')

    class Meta:
        model = models.Designation
        fields = {'name'}

    def filter_name(self, queryset, name, value):
        fields = [f for f in self.Meta.model._meta.get_fields() if isinstance(
            f, CharField
        )]

        queries = [Q(**{f.name + '__icontains': value}) for f in fields]
        qs = Q()
        for query in queries:
            qs = qs | query
        return self.Meta.model.objects.filter(qs)


class EmployeeFilter(FilterSet):

    class Meta:
        model = models.Employee
        fields = {'name': ['icontains']}


class CompanyKeyPersonFilter(FilterSet):

    class Meta:
        model = models.CompanyKeyPerson
        fields = {'name': ['icontains']}


class DistributorFilter(FilterSet):

    class Meta:
        model = models.Distributor
        fields = {'firm_name': ['icontains']}
