def generate_product_dict(obj, accessor):
    product = getattr(obj, accessor).all()
    product_dict = {
        "product_row-TOTAL_FORMS": str(product.count()),
        "product_row-INITIAL_FORMS": "0",
        "product_row-MIN_NUM_FORMS": "0",
        "product_row-MAX_NUM_FORMS": "1000",
    }

    count = 0

    for each_product in product:
        product_dict["product_row-"+str(count)+"-product"] = each_product.product
        product_dict["product_row-"+str(count)+"-product_quantity"] = each_product.product_quantity
        product_dict["product_row-"+str(count)+"-product_cases"] = each_product.product_cases
        product_dict["product_row-"+str(count)+"-product_weight"] = each_product.product_weight
        product_dict["product_row-"+str(count)+"-product_mrp"] = each_product.product_mrp

        count += 1

    return product_dict
