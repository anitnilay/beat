from django.conf.urls import url, include
from django.urls import path
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from django.http import HttpResponseRedirect
from work_management import views as customer_views

urlpatterns = [
    path('', lambda r: HttpResponseRedirect('master/')),
    path('admin/', admin.site.urls),
    path('login/', customer_views.user_login, name='login'),
    path('logout/', auth_views.logout, name='logout'),
    path('grappelli/', include('grappelli.urls')),
    path('master/', include('master.urls')),
    path('order/', include('order.urls'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
    + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)